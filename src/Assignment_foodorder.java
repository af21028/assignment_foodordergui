import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;

public class Assignment_foodorder {
    private JLabel toplabel;
    private JButton GyozaButton;
    private JButton SashimiButton;
    private JButton EdamameButton;
    private JButton SaladButton;
    private JButton PastaButton;
    private JButton ChickenButton;
    private JButton CheckoutButton;
    private JTextPane OrderList;
    private JLabel OrderdItems;
    private JPanel root;
    private JTextPane TotalPrice;
    private JButton CancelButton;


    Map<String, Integer> map = new HashMap<>();

    int total_price = 0;
    int items_num = 0;

    public void Order_Config(String food){


        int Config_num = JOptionPane.showConfirmDialog(
                null,
                "Would you like to order " + food + " ?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION
        );

        if (Config_num == 0){
            JOptionPane.showMessageDialog(null, "Thank you for ordering " + food + " ! It will be served as soon as possible");
            String currentText = OrderList.getText();
            OrderList.setText(currentText + food);

            int i=0;
            while (i < 15 - food.length() - String.valueOf(map.get(food)).length() ){
                currentText = OrderList.getText();
                OrderList.setText(currentText+ " ");
                ++i;
            }
            currentText = OrderList.getText();
            OrderList.setText(currentText + map.get(food) + "yen\n");
            total_price += map.get(food);
            items_num +=1;
            TotalPrice.setText("        " + items_num + " items\n"+ "Total   " + total_price + "yen");

        }
    }
    public Assignment_foodorder() {



        map.put("Gyoza", 450);
        map.put("Sashimi", 850);
        map.put("Edamame", 250);
        map.put("Salad", 650);
        map.put("Pasta", 1050);
        map.put("Chicken", 1250);


        GyozaButton.addActionListener(new ActionListener() {
            @Override public void actionPerformed(ActionEvent e) {
                Order_Config("Gyoza");

            }
        });

        SashimiButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Order_Config("Sashimi");
            }
        });

        EdamameButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Order_Config("Edamame");
            }
        });

        SaladButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Order_Config("Salad");
            }
        });

        PastaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Order_Config("Pasta");
            }
        });

        ChickenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Order_Config("Chicken");
            }
        });

        CheckoutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int Config_num = JOptionPane.showConfirmDialog(
                        null,
                        "Would you like to checkout ?",
                        "Checkout  Confirmation",
                        JOptionPane.YES_NO_OPTION
                );

                if (Config_num == 0) {
                    JOptionPane.showMessageDialog(null, "Thank you for ordering!The total items: " + items_num + " The total price is " + total_price + "yen");
                    OrderList.setText("");
                    total_price = 0;
                    items_num = 0;
                    TotalPrice.setText("Total  0yen");
                }
            }
        });
        CancelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                OrderList.setText("");
                TotalPrice.setText("Total  0yen");
                total_price = 0;
                items_num = 0;
            }
        });

        //ボタンのアイコン画像
        GyozaButton.setIcon(new ImageIcon(this.getClass().getResource("./gyoza.jpg")));
        SashimiButton.setIcon(new ImageIcon(this.getClass().getResource("./sashimi.jpg")));
        EdamameButton.setIcon(new ImageIcon(this.getClass().getResource("./edamame.jpg")));
        SaladButton.setIcon(new ImageIcon(this.getClass().getResource("./salad.jpg")));
        PastaButton.setIcon(new ImageIcon(this.getClass().getResource("./pasta.jpg")));
        ChickenButton.setIcon(new ImageIcon(this.getClass().getResource("./chicken.jpg")));

        //ボタンの中のテキスト

        GyozaButton.setText("Gyoza\s" + map.get("Gyoza") + "yen");
        SashimiButton.setText("Sashimi\s" + map.get("Sashimi") + "yen");
        EdamameButton.setText("Edamame\s" + map.get("Edamame") + "yen");
        SaladButton.setText("Salad\s" + map.get("Salad") + "yen");
        PastaButton.setText("Pasta\s" + map.get("Pasta") + "yen");
        ChickenButton.setText("Chicken\s" + map.get("Chicken") + "yen");


        //ボタンのフォントサイズ
        GyozaButton.setFont(new Font("Consolas", Font.PLAIN, 24));
        SashimiButton.setFont(new Font("Consolas", Font.PLAIN, 24));
        EdamameButton.setFont(new Font("Consolas", Font.PLAIN, 24));
        SaladButton.setFont(new Font("Consolas", Font.PLAIN, 24));
        PastaButton.setFont(new Font("Consolas", Font.PLAIN, 24));
        ChickenButton.setFont(new Font("Consolas", Font.PLAIN, 24));
        CancelButton.setFont(new Font("Consolas", Font.PLAIN, 18));

        //出力される文字のフォントサイズ
        toplabel.setFont(new Font("Consolas", Font.PLAIN, 30));
        OrderdItems.setFont(new Font("Consolas", Font.BOLD, 20));
        OrderList.setFont(new Font("Consolas", Font.BOLD, 25));
        TotalPrice.setFont(new Font("Consolas", Font.BOLD, 25));
        CheckoutButton.setFont(new Font("Consolas", Font.PLAIN, 20));



    }


    public static void main(String[] args) {
        JFrame frame = new JFrame("Assignment_foodorder");
        frame.setContentPane(new Assignment_foodorder().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }


}

